# Generated by Django 4.1.7 on 2023-03-01 23:49

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("todos", "0002_todoitem"),
    ]

    operations = [
        migrations.AlterField(
            model_name="todoitem",
            name="task",
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name="todolist",
            name="name",
            field=models.CharField(max_length=100),
        ),
    ]
